<?php
/**
 * Service permettant de récupèrer un flux XML et de sauvegarder les commandes en base de données
 *
 */

namespace TestBundle;


use Symfony\Bridge\Monolog\Logger;

class LengowTestService {


    /**
     * @var Symfony\Bridge\Monolog\Logger
     * Pour le logging
     */
    protected $logger;

    /**
     * @var string
     * Url du flux XML
     */
    protected $urlOrders;


    public function __construct($urlOrders, Logger $logger){
        $this->urlOrders = $urlOrders;
        $this->logger = $logger;
    }


    /**
     * Retourne le resultat du flux XML au format SimpleXML
     * @return \SimpleXMLElement
     */
    public function getXmlFeed(){

        //Appel de téléchargement
        $this->logger->info("Téléchargement du fichier XML " . $this->urlOrders);

        //Pour ne pas polluer avec warning
        libxml_use_internal_errors(true);
        $xml = simplexml_load_file($this->urlOrders);

        //Si le fichier n'a pas pu être parsé, on retourne false
        if(false === $xml){
            //Message log
            $this->logger->error("Erreur lors du téléchargement du fichier XML " . $this->urlOrders . ": Impossible de lire ou de parser le fichier XML.");
            return null;
        }
        else{
            $this->logger->info("Fichier XML " . $this->urlOrders . " téléchargé avec succès");
            return $xml;
        }
    }



} 