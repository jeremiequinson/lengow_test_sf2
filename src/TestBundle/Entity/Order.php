<?php

namespace TestBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * @ORM\Entity(repositoryClass="TestBundle\Entity\OrderRepository")
 * @ORM\Table(name="orders")
 *
 * @GRID\Source(columns="id, orderId, marketplace, orderStatusMarketplace, orderAmount")
 */
class Order {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @GRID\Column(title="id", filterable=false, filter=false, visible=false)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @GRID\Column(title="Numéro de commande", filterable=true, filter=true, visible=true)
     */
    protected $orderId;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @GRID\Column(title="Vendeur", filterable=true, filter=true, visible=true)
     */
    protected $marketplace;

    /**
     * @ORM\Column(type="string", length=255)
     * @GRID\Column(title="Status", filterable=true, filter=true, visible=true)
     */
    protected $orderStatusMarketplace;


    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * @GRID\Column(title="orderAmount", filterable=true, filter=true, visible=true)
     */
    protected $orderAmount;







    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return Order
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Order
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderStatusMarketplace
     *
     * @param string $orderStatusMarketplace
     * @return Order
     */
    public function setOrderStatusMarketplace($orderStatusMarketplace)
    {
        $this->orderStatusMarketplace = $orderStatusMarketplace;

        return $this;
    }

    /**
     * Get orderStatusMarketplace
     *
     * @return string 
     */
    public function getOrderStatusMarketplace()
    {
        return $this->orderStatusMarketplace;
    }

    /**
     * Set orderAmount
     *
     * @param string $orderAmount
     * @return Order
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return string 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }


}
