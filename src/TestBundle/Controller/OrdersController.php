<?php
/**
 * Created by Jérémie Quinson
 * Date: 22/09/15
 * Time: 20:34
 */

namespace TestBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Yaml\Dumper;
use TestBundle\Entity\Order;
use APY\DataGridBundle\Grid\Source\Entity;

class OrdersController extends Controller{




    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function IndexAction(){

        return $this->render(
            'TestBundle:Orders:index.html.twig'
        );
    }





    /**
     * Action: Parser le flux XML et enregistrer les commandes en base de données
     */
    public function ImportAction(){

        //On importe le flux
        $xml = $this->get('lengow_test')->getXmlFeed();
        $errorClient = "";
        $countAddedOrders = 0;

        //Si le fichier n'est pas vide
        if(!empty($xml)){

            //Si le format est bon
            if(!empty($xml->orders->order)){

                $items = $xml->orders->order;
                $em = $this->getDoctrine()->getManager();
                $orderRep = $em->getRepository('TestBundle:Order');

                $em->getConnection()->beginTransaction();

                try{
                    //Pour chaque item, on vérifie si le numéro de commande n'existe pas et on ajoute la commande en BDD
                    foreach($items as $item) {

                        //Si numéro de commande vide ou si la commande existe déjà
                        if($item->order_id === null || $orderRep->orderExists($item->order_id)){
                            continue;
                        }

                        $order = new Order();
                        $order->setMarketplace($item->marketplace);
                        $order->setOrderId($item->order_id);
                        $order->setOrderStatusMarketplace($item->order_id);
                        $order->setOrderAmount($item->order_amount);

                        $em->persist($order);
                        $countAddedOrders++;
                    }

                    $em->flush();
                    $em->getConnection()->commit();
                }
                catch(Exception $e){
                    $em->getConnection()->rollback();
                    $errorClient = "Une erreur inconnue s'est produite.";
                    $this->get('logger')->error("Une erreur s'est produite durant l'import des flux: " . $e->getMessage());
                }

            }
            else{
                $errorClient = "Flux XML: format incorrect";
            }

        }
        else{
            $errorClient = "Impossible de lire le flux XML.";
        }



        return $this->render(
            'TestBundle:Orders:import.html.twig',
            array(
                'errorClient'      => $errorClient,
                'countAddedOrders' => $countAddedOrders
            )
        );
    }


    /**
     * Liste des commandes
     */
    public function ListAction(){

        //Instance du service avec la source Order
        $source = new Entity('TestBundle:Order');
        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse('TestBundle:Orders:list.html.twig');
    }


    /**
     * Liste des commandes
     */
    public function ApiListAction($id){

        $em = $this->getDoctrine()->getManager();

        $format = "";

        //Si le paramètre pour le format est définie, on récupère la valeur
        if($this->container->hasParameter('api_format')){
            $api_format = $this->container->getParameter('api_format');

            if(isset($api_format['orders'])){
                $format = $api_format['orders'];
            }
        }

        //Test. Si le nom du format n'est pas valide, on rendra du json
        if(empty($format)){
            $format = "json";
        }


        //Si un id est fourni
        if(!empty($id)){

            $order = $em->getRepository("TestBundle:Order")->findArray($id);

            //Si aucune commande n'a été trouvée
            if (!$order) {
                throw $this->createNotFoundException("La commande n'existe pas");
            }

            //Selon le format souhaité, on parse en Json ou en Yaml
            if($format == "yaml")
            {
                $dumper = new Dumper();
                $yaml = $dumper->dump($order, 2);
                return new Response("<pre>" . $yaml . "</pre>");
            }
            else
            {
                return new JsonResponse($order);
            }
        }
        else{
            //Si aucun id, on récupère la liste des commandes
            $orders = $em->getRepository("TestBundle:Order")->findAllArray();

            //Selon le format souhaité, on parse en Json ou en Yaml
            if($format == "yaml")
            {
                $dumper = new Dumper();
                $yaml = $dumper->dump($orders, 2);
                return new Response("<pre>" . $yaml . "</pre>");
            }
            else
            {
                return new JsonResponse($orders);
            }
        }
    }




    /**
     * Liste des commandes
     */
    public function NewAction(Request $request){

        $order = new Order();

        //formulaire de création
        $form = $this->createFormBuilder($order)
            ->add('orderId', 'text', array('label'  => 'Numéro de commande'))
            ->add('marketplace', 'text', array('label'  => 'Vendeur'))
            ->add('orderStatusMarketplace', 'text', array('label'  => 'Status'))
            ->add('orderAmount', 'number', array('label'  => 'Montant'))
            ->getForm();

        //On bind les paramètres post qui vont bien dans le formulaire
        $form->handleRequest($request);

        //Si le formulaire est valide, on créé une nouvelle commande avec les données du formulaire
        if ($form->isValid()) {

            //Persist
            try{
                $em = $this->getDoctrine()->getManager();
                $em->persist($order);
                $em->flush();

                //Si aucune erreur ne s'est produite, on redirige vers la page de liste + message succes
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'La commande a bien été créée!'
                );

                return $this->redirect($this->generateUrl('test_list'));
            }
            catch(Exception $e){

                $this->get('logger')->error("Une erreur s'est produite lors de la création d'une commande: " . $e);

                $this->get('session')->getFlashBag()->add(
                    'error',
                    "La commande n'a pas pu être créée à cause d'une erreur !"
                );
            }
        }


        //On affiche le formulaire
        return $this->render('TestBundle:Orders:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }


} 